#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Generates a dashboard with plotly and dash.

Uses filters, radiobuttons and checkboxes, makes the resulting website
available at http://127.0.0.1:8050/
"""
import base64
import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import json
import pandas as pd
import plotly.graph_objs as go

json_data_file = "./data.json"

with open(json_data_file) as f:
    data = json.load(f)

meta_tags = [
    {"name": "viewport",
     "content": "width=device-width, initial-scale=1.0"}]
app = dash.Dash(__name__, static_folder='assets', meta_tags=meta_tags)
server = app.server

###############################################################################
# START funnel chart                                                          #
###############################################################################

f_phases = data['funnel']['phases']
f_values = data['funnel']['values']
f_colors = data['funnel']['colors']
funnel_data = np.array([f_phases, f_values]).T

n_phase = len(f_phases)
plot_width = 400

# height of a section and difference between sections 
section_h = 100
section_d = 10

# multiplication factor to calculate the width of other sections
unit_width = plot_width / max(f_values)

# width of each funnel section relative to the plot width
phase_w = [int(value * unit_width) for value in f_values]

# plot height based on the number of sections and the gap in between them
height = section_h * n_phase + section_d * (n_phase - 1)

# list containing all the plot shapes
shapes = []

# list containing the Y-axis location for each section's name and value text
label_y = []

for i in range(n_phase):
    if (i == n_phase-1):
        points = [phase_w[i] / 2, height, phase_w[i] / 2, height - section_h]
    else:
        points = [phase_w[i] / 2, height, phase_w[i+1] / 2, height - section_h]

    path = 'M {0} {1} L {2} {3} L -{2} {3} L -{0} {1} Z'.format(*points)

    shape = {
        'type': 'path',
        'path': path,
        'fillcolor': f_colors[i],
        'line': {
            'width': 1,
            'color': f_colors[i]
        }
    }
    shapes.append(shape)

    # Y-axis location for this section's details (text)
    label_y.append(height - (section_h / 2))

    height = height - (section_h + section_d)

# For phase names
label_trace = go.Scatter(
    x=[-320]*n_phase,
    y=label_y,
    mode='text',
    textposition='middle right',
    text=f_phases,
    textfont=dict(
#        color='#00a1cd',
        size=15
    )
)

# For phase values
value_trace = go.Scatter(
    x=[255]*n_phase,
    y=label_y,
    mode='text',
    textposition='middle left',
    text=f_values,
    textfont=dict(
#        color='#00a1cd',
        size=15
    )
)

f_data = [label_trace, value_trace]

f_layout = go.Layout(
    shapes=shapes,
    showlegend=False,
#    paper_bgcolor='#cad2d3',
#    plot_bgcolor='#cad2d3',
    margin=go.Margin(l=0, b=0, t=0, r=0),
    xaxis=dict(
        showgrid=False,
        showticklabels=False,
        zeroline=False,
        automargin=True,
    ),
    yaxis=dict(
        showgrid=False,
        showticklabels=False,
        zeroline=False,
        automargin=True,
    )
)

###############################################################################
# END funnel chart                                                            #
###############################################################################

###############################################################################
# START pie chart                                                             #
###############################################################################

sum_values = sum(data["os"]["iOS"]["values"] + data["os"]["Android"]["values"])
ios_share = sum(data["os"]["iOS"]["values"]) / sum_values
android_share = sum(data["os"]["Android"]["values"]) / sum_values

pie_fig = {
    "data":
    [
        {
            "values": data["os"]["iOS"]["values"] + \
                data["os"]["Android"]["values"],
            "labels": data["os"]["iOS"]["versions"] + \
                data["os"]["Android"]["versions"],
            "marker":
            {
                "colors": data["os"]["iOS"]["pie_colors"] \
                       + data["os"]["Android"]["pie_colors"],
                "line": {"width": 2, "color": "white"}
            },
            "hoverinfo": "label+percent",
            "textinfo": "percent",
            "insidetextfont": {"color": "white"},
            "textposition": "inside",
            "hole": .4,
            "type": "pie",
            "sort": False,
            "direction": "clockwise",
        }
    ],
    "layout":
    {
        "margin": go.Margin(l=5, b=5, t=5, r=5),
        "showlegend": False,
        "annotations":
        [
            {
                "text": f"<b>iOS: {ios_share*100:.1f}%</b>",
                "yshift": -10,
                "showarrow": False,
                "font":
                {
                    "color": data["os"]["iOS"]["pie_colors"][0],
                }
            },
            {
                "text": f"<b>And: {android_share*100:.1f}%</b>",
                "yshift": 10,
                "showarrow": False,
                "font":
                {
                    "color": data["os"]["Android"]["pie_colors"][0],
                }
            }
        ]
    }
}

###############################################################################
# END pie chart                                                               #
###############################################################################

###############################################################################
# START response chart                                                        #
###############################################################################

response_data = np.array(data["response"]["data"])
response_colors = data["response"]["colors"]
response_data_df = pd.DataFrame(data=response_data[1:,0:],
                         columns=response_data[0,0:])
response_data_df.set_index("Date", inplace=True)
dates = response_data_df.index

plot_data = []

for column in response_data_df:
    trace = go.Scatter(
        x = dates,
        y = response_data_df[column],
        name = column,
        line = {"color": response_colors[column][0]},
        fill='tozeroy',
        fillcolor=response_colors[column][1],
    )
    plot_data.append(trace)

response_layout = {
#    "xaxis": {"rangeslider": True},
    "type": "date",
    "margin": go.Margin(l=30, b=0, t=0, r=0),
    "legend": {"orientation": "h"},
#    "paper_bgcolor":"red",
#    "plot_bgcolor":"yellow",
}

response_fig = {"data": plot_data, "layout": response_layout}

###############################################################################
# END response chart                                                          #
###############################################################################

###############################################################################
# START questionnaire chart                                                   #
###############################################################################

questionnaire_data = np.array(data["questionnaire"]["data"])
questionnaire_colors = data["questionnaire"]["colors"]
questionnaire_data_df = pd.DataFrame(data=questionnaire_data[1:,0:],
                         columns=questionnaire_data[0,0:])
questionnaire_data_df.set_index("Date", inplace=True)
dates = questionnaire_data_df.index

plot_data = []

for column in questionnaire_data_df:
    trace = go.Scatter(
        x = dates,
        y = questionnaire_data_df[column],
        name = column,
        line = {"color": questionnaire_colors[column][0]},
        fill='tozeroy',
        fillcolor=questionnaire_colors[column][1],
    )
    plot_data.append(trace)

questionnaire_layout = {
#    "xaxis": {"rangeslider": True},
    "type": "date",
    "margin": go.Margin(l=30, b=0, t=10, r=0),
    "legend": {"orientation": "h"}
}

questionnaire_fig = {"data": plot_data, "layout": questionnaire_layout}

###############################################################################
# END questionnaire chart                                                     #
###############################################################################

###############################################################################
# START stop chart                                                            #
###############################################################################

stop_data = np.array(data["stop"]["data"])
stop_colors = data["stop"]["colors"]
stop_data_df = pd.DataFrame(data=stop_data[1:,0:],
                         columns=stop_data[0,0:])
stop_data_df.set_index("Date", inplace=True)
dates = stop_data_df.index

plot_data = []

for column in stop_data_df:
    trace = go.Scatter(
        x = dates,
        y = stop_data_df[column],
        name = column,
        line = {"color": stop_colors[column][0]},
        fill='tozeroy',
        fillcolor=stop_colors[column][1],
    )
    plot_data.append(trace)

stop_layout = {
#    "xaxis": {"rangeslider": True},
    "type": "date",
    "margin": go.Margin(l=30, b=0, t=10, r=0),
    "legend": {"orientation": "h"}
}

stop_fig = {"data": plot_data, "layout": stop_layout}

###############################################################################
# END stop chart                                                              #
###############################################################################

###############################################################################
# START trip chart                                                            #
###############################################################################

trip_data = np.array(data["trip"]["data"])
trip_colors = data["trip"]["colors"]
trip_data_df = pd.DataFrame(data=trip_data[1:,0:],
                         columns=trip_data[0,0:])
trip_data_df.set_index("Date", inplace=True)
dates = trip_data_df.index

plot_data = []

for column in trip_data_df:
    trace = go.Scatter(
        x = dates,
        y = trip_data_df[column],
        name = column,
        line = {"color": trip_colors[column][0]},
        fill='tozeroy',
        fillcolor=trip_colors[column][1],
    )
    plot_data.append(trace)

trip_layout = {
#    "xaxis": {"rangeslider": True},
    "type": "date",
    "margin": go.Margin(l=30, b=0, t=10, r=0),
    "legend": {"orientation": "h"}
}

trip_fig = {"data": plot_data, "layout": trip_layout}

###############################################################################
# END trip chart                                                              #
###############################################################################

###############################################################################
# START summary table                                                         #
###############################################################################

summary_data = np.array(data["summary"])
summary_df = pd.DataFrame(data=summary_data[1:, 0:],
                          columns=summary_data[0, 0:])
summary_data = summary_df.to_dict('rows')
summary_columns = [{'id': c, 'name': c} for c in summary_df.columns]

###############################################################################
# END summary table                                                           #
###############################################################################

height_first_row = "35vh"
height_second_row = "25vh"

# generate HTML
# pylint: disable=no-member
app.layout = html.Div([
    # Base CSS taken from: https://codepen.io/chriddyp/pen/bWLwgP.css
    html.Link(href='./assets/main.css', rel='stylesheet'),
    # Override base CSS to customize colors etc.
    html.Link(href='./assets/changes.css', rel='stylesheet'),
    html.Div([
        html.Div([
            html.Div([
                html.Div([
                    html.H3('CBS Verplaatsingen Dashboard'),
                ],
                         style={"float": "left", "margin": "0 25px", "text-align": "center"},
                         ),
                html.Div(
                    html.Img(
                        src=app.get_asset_url("cbs-brand.svg"),
                        style={"height": "50px", "position": "absolute", "right": "8px", "top": "50%", "margin-top": "-37px", "padding": "10px"},
                    ),
                    style={"float": "right"},
                ),
            ],style={'position': 'absolute', 'left': '-10px', 'background': 'white', 'border-radius': '0px 10px 10px 0px', 'box-shadow': '5px 5px 5px grey'}, className="six columns"),
            html.H3('CBS Verplaatsingen Dashboard'),
        ], className="six columns"),
    ], className="row"),
    html.Div([
        html.Div([
            html.Div([
                html.H3('Response', className="box-header"),
                dcc.Graph(
                    figure=response_fig,
                    id="response",
                    style={'height': height_first_row}
                )
            ], className="box-content"),
        ], className="five columns box-wrapper"),
        html.Div([
            html.Div([
                html.H3('OS shares', className="box-header"),
                dcc.Graph(
                    figure=pie_fig,
                    id="pie",
                    style={'height': height_first_row}
                )
            ], className="box-content"),
        ], className="three columns box-wrapper"),
        html.Div([
            html.Div([
                html.H3('Funnel', className="box-header"),
                dcc.Graph(
                    figure=go.Figure(data=f_data, layout=f_layout),
                    id="funnel",
                    style={'height': height_first_row}
                )
            ], className="box-content"),
        ], className="four columns box-wrapper"),
    ], className="row"),
    html.Div([
        html.Div([
            html.Div([
                html.H3('Stops', className="box-header"),
                dcc.Graph(
                    figure=stop_fig,
                    id="stops",
                    style={'height': height_second_row}
                )
            ], className="box-content"),
        ], className="three columns box-wrapper"),
        html.Div([
            html.Div([
                html.H3('Trips', className="box-header"),
                dcc.Graph(
                    figure=trip_fig,
                    id="trips",
                    style={'height': height_second_row}
                )
            ], className="box-content"),
        ], className="three columns box-wrapper"),
        html.Div([
            html.Div([
                html.H3('Questionnaires', className="box-header"),
                dcc.Graph(
                    figure=questionnaire_fig,
                    id="questionnaire",
                    style={'height': height_second_row}
                )
            ], className="box-content"),
        ], className="three columns box-wrapper"),
        html.Div([
            html.Div([
                html.H3('Summary', className="box-header"),
                html.Div([
                    dash_table.DataTable(
                        id="summary",
                        data=summary_data,
                        columns=summary_columns,
                        style_table={'maxHeight': height_second_row, "overflowY": 'scroll', "font-size": "large"},
                        style_header={'display': 'none'},
                        style_cell_conditional=[{
                            "if": {"column_id": "stats"},
                            "textAlign": "left",
                            "fontWeight": "bold",
                        },
                        {
                            "if": {"column_id": "values"},
                            "color": "#82045e",
                        }],
                        style_as_list_view=True,
                    ),
                ], style={"height": height_second_row}),
            ], className="box-content", style={"margin": "0 auto"}),
        ], className="three columns box-wrapper"),
    ], className="row"),
])

app.css.config.serve_locally = True
app.scripts.config.serve_locally = True

app.title = "Tabi Dashboard"


if __name__ == '__main__':
    app.run_server(debug=True)
